import React, { Component } from 'react';
import Gallery from 'react-grid-gallery';
import Lightbox from 'react-images';

import fullscreen from './fullscreen.svg';

import './App.css';
import './GridView.css';
import './Panel.css';


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      response: '',
      images: '',
      lightboxIsOpen: false,
      currentImage: 0,
    };

    this.categories = [ 'smile', 'people present', 'visible sky', 'other' ];
    this.actions = ['Select All', 'Deselect All', 'Clear Tags'];

    this.handleViewClick = this.handleViewClick.bind(this);
    this.goToPrevious = this.goToPrevious.bind(this);
    this.goToNext = this.goToNext.bind(this);
    this.closeLightbox = this.closeLightbox.bind(this);
  }

  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ images: res.images }))
      .catch(err => console.log(err));
  }

  callApi = async () => {
    const response = await fetch('/api/home');
    const body = await response.json();

    if (response.status !== 200) throw Error(body.message);

    return body;
  }


  /*************************** PANEL ***************************/


  handleCategoryClick(category, event) {
    let images = this.state.images.slice();
    images.forEach(image => {
      if (image.isSelected) {
        image.isSelected = false;
        image.category = category;
      }

      return image;
    })

    this.setState({
      images: images
    });
  }

  handleViewClick(event) {
    let images = this.state.images.slice();

    images.forEach(image => {
      if (event.target.value === 'all') {
        image.visible = true;
      } else {
        image.visible = (image.category === event.target.value);
      }
      return image;
    });

    this.setState({
      images: images
    });
  }

  handleActionClick(action, event) {
    let images = this.state.images.slice();

    images.forEach(image => {
      if (action === 'Select All') {
        image.isSelected = image.visible ? true : image.isSelected;
      } else if (action === 'Deselect All') {
        image.isSelected = image.visible ? false : image.isSelected;
      } else if (action === 'Clear Tags') {
        image.category = image.visible ? image.category = '' : image.category;
      }

      return image;
    });

    this.setState({
      images: images
    });
  }

  renderPanel() {
    let options = this.categories.map(category => <option value={category}>{category}</option>);

    let panelButtons = this.categories.map(category =>
      <button key={category} className="category-button" onClick={this.handleCategoryClick.bind(this, category)}>
        {category}
      </button>
    );

    let actionButtons = this.actions.map(action =>
      <button key={action} className="action-button" onClick={this.handleActionClick.bind(this, action)}>
        {action}
      </button>
    );

    return (
      <div className="Panel">
        {panelButtons}
        <div className="select-container">
          <label htmlFor="view">View tags:</label>
          <select name="view" onChange={this.handleViewClick}>
            <option value="all">all</option>
            {options}
          </select>
        </div>
        <div className="action-button-wrapper">
          {actionButtons}
        </div>
      </div>
    );
  }


  /*************************** GRID VIEW ***************************/


  onClickThumbnail(index, event) {
    let images = this.state.images.slice();
    let selectedImage = images[index];
    selectedImage.isSelected = !selectedImage.isSelected;

    this.setState({
      images: images
    });
  }

  onClickFullscreen(index, event) {
    event.stopPropagation();
    let targetImage = this.state.images[index];
    let visibleImages = this.state.images.filter(image => image.visible === true);

    this.setState({
      lightboxIsOpen: true,
      currentImage: visibleImages.indexOf(targetImage)
    });
  }

  renderThumbnails(image, index) {
    if (!image.visible) { return null; }

    let className = "thumbnail-container";
    className = image.isSelected ? className + " selected" : className;
    className = image.category ? className + " tagged" : className;

    return (
      <div className={className} onClick={this.onClickThumbnail.bind(this, index)} key={index}>
        <img className="fullscreen" onClick={this.onClickFullscreen.bind(this, index)} src={fullscreen}/>
        {image.category ? <div className="tag">{image.category}</div> : null}
        <img className="thumbnail" src={image.src}/>
      </div>
    )
  }

  renderGridView() {
    if (this.state.images) {
      return (
        <div className="GridView">
          {this.state.images.map((image, index) => this.renderThumbnails(image, index))}
        </div>
      );
    }

    return null;
  }

  goToPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1
    })
  }

  goToNext() {
    this.setState({
      currentImage: this.state.currentImage + 1
    })
  }

  closeLightbox() {
    this.setState({
      lightboxIsOpen: false
    })
  }

  handleLightboxClick(category, event) {
    let images = this.state.images.slice();
    let visibleImages = images.filter(image => image.visible === true);
    let targetImage = visibleImages[this.state.currentImage];

    targetImage.category = targetImage.category === category ? '' : category;

    this.setState({
      images: images
    });
  }

  renderLightbox() {
    if (this.state.images && this.state.lightboxIsOpen) {
      let images = this.state.images.filter(image => image.visible === true);

      let lightboxButtons = this.categories.map(category => {

        const currentCategory = images[this.state.currentImage].category;
        let className = currentCategory === category ? "lightbox-button selected" : "lightbox-button";

        return (
          <button key={category} className={className} onClick={this.handleLightboxClick.bind(this, category)}>
            {category}
          </button>
        );
      });

      let controlBar = [<div className="control-bar">{lightboxButtons}</div>];

      return (
        <Lightbox
          backdropClosesModal={true}
          currentImage={this.state.currentImage}
          customControls={controlBar}
          images={images}
          isOpen={this.state.lightboxIsOpen}
          onClickNext={this.goToNext}
          onClickPrev={this.goToPrevious}
          onClose={this.closeLightbox}
          showCloseButton={false}
        />
      )
    }

    return null;
  }

  render() {
    return (
      <div className="App">
        {this.renderGridView()}
        {this.renderPanel()}
        {this.renderLightbox()}
      </div>
    );
  }
}

export default App;
