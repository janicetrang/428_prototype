# ONLINE RESOURCES
## Tutorial on how to combine create-react-app, NodeJS and and Express: https://medium.freecodecamp.org/how-to-make-create-react-app-work-with-a-node-backend-api-7c5c48acb1b0

## Github repo for the above tutorial:
https://github.com/esausilva/example-create-react-app-express

## Github repo for create-react-app:
https://github.com/facebook/create-react-app


# OVERVIEW
/client: directory containing the client-side code, mostly from the
         create-react-app

	/public: contains the public resources, e.g. the compiled HTML index.html
	/src: contains source code, including JS and CSS

/server.js: simple Express server, which will handle API calls (if we need to)


# HOW TO RUN THE PROTOTYPE

Follow the set-up instructions at https://github.com/esausilva/example-create-react-app-express
