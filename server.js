const express = require('express');
const fs = require('fs');

const app = express();
const port = process.env.PORT || 5000;

function getImages() {
  const imgEnv = 'demo';
  const folder = `./client/public/${imgEnv}/`;

  return fs.readdirSync(folder).map(name => {
    let obj = {};
    const srcPath = `/${imgEnv}/` + name;

    obj.src = srcPath;
    obj.isSelected = false;
    obj.category = '';
    obj.visible = true;

    return obj;
  });
}

app.get('/api/home', (req, res) => {
  let images = getImages();
  res.send({ images: images });
});

app.listen(port, () => console.log(`Listening on port ${port}`));
